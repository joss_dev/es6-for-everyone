import { showModal } from "./modal";
import { createFighterDetails } from "./fighterDetails";

export  function showWinnerModal(fighter) {
    const bodyElement = createFighterDetails(fighter);
    showModal({ title: "Winner", bodyElement });
  // show winner name and image
}
